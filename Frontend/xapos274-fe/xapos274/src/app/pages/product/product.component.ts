import { HttpErrorResponse } from '@angular/common/http';
import { ClassGetter } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Category } from 'src/app/models/category';
import { Product } from 'src/app/models/product';
import { Variant } from 'src/app/models/variant';
import { CategoryService } from 'src/app/services/category.service';
import { ProductService } from 'src/app/services/product.service';
import { VariantService } from 'src/app/services/variant.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  public product: any = [];
  public variant: any = [];
  public category: any = [];
  public editProduct: Product;
  public deleteProduct: Product;

  constructor(
    private productService: ProductService,
    private variantService: VariantService,
    private categoryService: CategoryService,
  ) { 
    this.editProduct = {} as Product;
    this.deleteProduct = {} as Product;
  }

  ngOnInit(): void {
    this.getProductAPI();
  }

  public getProductAPI(): void {
    this.productService.getProduct().subscribe(
      (response: any) => {
        this.product = response.result;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
  }

  public searchProductAPI(search : any): void {
    this.productService.searchProduct(search).subscribe(
      (response: any) => {
        this.product = response.result;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
  }


  public getVariantAPI(): void {
    this.variantService.getVariant(1, 5, 'varian_code', 'ASC').subscribe(
      (response: any) => {
        this.variant = response.result;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
  }

  public getVariantFromCategoryAPI(category_id : any): void {
    this.variantService.getVariantFromCategory(category_id).subscribe(
      (response: any) => {
        this.variant = response.result;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
  }

  public getCategoryAPI(): void {
    this.categoryService.getCategory().subscribe(
      (response: any) => {
        this.category = response.result;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
  }
  

  public onOpenModal(p: Product, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button')

    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-bs-toggle', 'modal');

    if (mode === 'add') {
      button.setAttribute('data-bs-target', '#addProductModal');
      this.getCategoryAPI();
    }

    if (mode === 'edit') {
      this.editProduct = p;
      console.log(p)
      button.setAttribute('data-bs-target', '#editProductModal')
      this.getCategoryAPI();
      this.getVariantFromCategoryAPI(this.editProduct.category_id);
    }

    if (mode === 'delete') {
      this.deleteProduct = p;
      button.setAttribute('data-bs-target', '#deleteProductModal')
    }

    container!.appendChild(button);
    button.click();
  }

  public onAddProduct(addForm: NgForm): void {
    document.getElementById('add-product-form')?.click();
    this.productService.addProduct(addForm.value).subscribe(
      (response: Product) => {
        this.getProductAPI();
        addForm.reset();
      }
    );
  }

  public onEditProduct(product: Product): void {
    this.productService.editProduct(product).subscribe(
      (response: Product) => {
        this.getProductAPI();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public onDeleteProduct(id: number): void {
    this.productService.deleteProduct(id).subscribe(
      () => {
        this.getProductAPI();
      },
      (error: HttpErrorResponse) => {
        alert(error.message)
      }
    );
  }
  
}
