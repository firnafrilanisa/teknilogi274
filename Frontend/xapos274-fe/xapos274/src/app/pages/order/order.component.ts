import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { OrderDetail } from 'src/app/models/orderdetail';
import { OrderHeader } from 'src/app/models/orderheader';
import { Product } from 'src/app/models/product';
import { OrderService } from 'src/app/services/order.service';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  public orderReference: any;
  public product: Product[] = [];
  public priceByProduct = 0;
  public orderDetail: OrderDetail[] = [];
  public totalAmount = 0;

  constructor(
    private orderService: OrderService,
    private productService: ProductService,
    private route: Router,
  ) { }

  ngOnInit(): void {
  }

  public newOrder() {
    this.orderService.postOrderHeader().subscribe(
      (response: any) => {
        this.getOrderReferenceAPI();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
    const container = document.getElementById('main-container');
    const button = document.createElement('button')
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-bs-toggle', 'modal');
    button.setAttribute('data-bs-target', '#addOrderModal');

    container!.appendChild(button);
    button.click();
  }

  public getOrderReferenceAPI(): void {
    this.orderService.getOrderReference().subscribe(
      (response: any) => {
        this.orderReference = response.result;
        this.getProductAPI();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
  }

  public getProductAPI() {
    this.productService.getProduct().subscribe(
      (response: any) => {
        this.product = response.result;
      }
    ),
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
  }

  public getPriceByProduct(id: any) {
    this.productService.getProductById(id).subscribe(
      (response: any) => {
        this.priceByProduct = response.result.product_price;
      }
    ),
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
  }

  public getOrderDetailByIdAPI(header_id: number) {
    this.orderService.getOrderDetailById(header_id).subscribe(
      (response: any) => {
        this.orderDetail = response.result;
        let totals = 0;

        for (let index = 0; index < this.orderDetail.length; index++) {
          totals = (this.orderDetail[index].price * this.orderDetail[index].quantity)
        }

        this.totalAmount += totals;
      }
    ),
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
  }

  public onAddOrderDetail(addForm: NgForm): void {
    this.orderService.addOrderDetail(addForm.value).subscribe(
      (response: any) => {
        this.getOrderDetailByIdAPI(addForm.value.header_id)
      }
    ),
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
  }

  public doneProcess() {
    let headerId = this.orderReference.id;
    this.orderService.doneProcessOrder(headerId, this.totalAmount).subscribe(
      (response: any) => {
        const container = document.getElementById('main-container');
        const button = document.createElement('button')
        button.type = 'button';
        button.style.display = 'none';
        button.setAttribute('data-bs-toggle', 'modal');
        button.setAttribute('data-bs-target', '#orderConfirmModal');
    
        container!.appendChild(button);
        button.click();
      }
    ),
    (error: HttpErrorResponse) => {
      alert(error.message);
    }
  }

  public goToOrder() {
    this.route.navigate(['order']).then(() => {
      window.location.reload();
    })
  }

}
