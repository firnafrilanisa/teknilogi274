import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Variant } from 'src/app/models/variant';
import { CategoryService } from 'src/app/services/category.service';
import { VariantService } from 'src/app/services/variant.service';

@Component({
  selector: 'app-variant',
  templateUrl: './variant.component.html',
  styleUrls: ['./variant.component.css']
})
export class VariantComponent implements OnInit {
  public variant: any = [];
  public category: any = [];
  public editVariant: Variant;
  public deleteVariant: Variant;
  public current_page = 1
  public records_per_page = 5
  public sortBy = 'variant_code'
  public mode = 'ASC'

  constructor(
    private variantService: VariantService,
    private categoryService: CategoryService
  ) {
    this.editVariant = {} as Variant;
    this.deleteVariant = {} as Variant;
  }

  ngOnInit(): void {
    this.getVariantAPI();
  }

  public getCategoryAPI(): void {
    this.categoryService.getCategory().subscribe(
      (response: any) => {
        this.category = response.result;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public nextPage() {
    if (this.current_page < this.totalPages()) {
      this.current_page++;
    }
  }

  public prevPage() {
    if (this.current_page > 1) {
      this.current_page--;
    }
  }

  public totalPages() {
    return Math.ceil(this.variant.length / this.records_per_page);
  }

  public getVariantAPI(): void {
    this.variantService.getVariant(this.current_page, this.records_per_page, this.sortBy, this.mode).subscribe(
      (response: any) => {
        this.variant = response.result;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public searchVariantAPI(search : any): void {
    this.variantService.searchVariant(search).subscribe(
      (response: any) => {
        this.variant = response.result;
      },
    );
  }

  public onOpenModal(v: Variant, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button')

    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-bs-toggle', 'modal');

    if (mode === 'add') {
      button.setAttribute('data-bs-target', '#addVariantModal');
      this.getCategoryAPI();
    }

    if (mode === 'edit') {
      this.editVariant = v;
      console.log(v)
      button.setAttribute('data-bs-target', '#editVariantModal')
      this.getCategoryAPI();
    }

    if (mode === 'delete') {
      this.deleteVariant = v;
      button.setAttribute('data-bs-target', '#deleteVariantModal')
    }

    container!.appendChild(button);
    button.click();
  }

  public onAddVariant(addForm: NgForm): void {
    document.getElementById('add-variant-form')?.click();

    this.variantService.addVariant(addForm.value).subscribe(
      (response: Variant) => {
        this.getVariantAPI();
        addForm.reset();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        addForm.reset();
      }
    )
  }

  public onEditVariant(variant: Variant): void {
    this.variantService.editVariant(variant).subscribe(
      (response: Variant) => {
        this.getVariantAPI();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
  }

  public onDeleteVariant(id: number): void {

    this.variantService.deleteVariant(id).subscribe(
      () => {
        this.getVariantAPI();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public select_records_per_page(value: number) {
    this.records_per_page = value;
    this.getVariantAPI()
    console.log(this.records_per_page)
  }

  public setSortBy(value: string) {
    this.sortBy = value;
    this.getVariantAPI();
  }

  public setMode(value: string) {
    this.mode = value;
    this.getVariantAPI();
  }

}
