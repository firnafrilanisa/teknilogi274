export interface Variant {
    id: number;
    variant_code: string;
    variant_name: string;
    category_id: number;
}