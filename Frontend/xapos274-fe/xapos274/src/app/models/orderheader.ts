export interface OrderHeader {
    id: number;
    reference: string;
    amount: string;
}