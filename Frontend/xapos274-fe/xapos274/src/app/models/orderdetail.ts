export interface OrderDetail {
    id: number;
    header_id: number;
    price: any;
    product_id: number;
    quantity: number;
    reference: any;
    product_name: any;
}