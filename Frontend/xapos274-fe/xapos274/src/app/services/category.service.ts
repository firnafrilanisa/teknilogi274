import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Config } from '../config/baseurl';
import { Observable } from 'rxjs';
import { Category } from '../models/category';

@Injectable({
  providedIn: 'root',
})
export class CategoryService {

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    }),
  };

  public searchCategory(search : any): Observable<Category[]> {
    return this.http.get<Category[]>(
      `${Config.url}api/category/${search}`,
      this.httpOptions
    ) 
  }

  public getCategory(): Observable<Category[]> {
    return this.http.get<Category[]>(
      `${Config.url}api/category`,
      this.httpOptions
    )
  }

  public sortingCategory(sortedBy: string, mode: string): Observable<Category[]> {
    return this.http.get<Category[]>(
      `${Config.url}api/category/${sortedBy}/${mode}`,
      this.httpOptions
    )
  }

  public addCategory(category: Category): Observable<Category> {
    return this.http.post<Category>(
      Config.url + 'api/addcategory',
      category, {
      ...this.httpOptions,
      responseType: 'text' as 'json',
      }
    )
  }

  public editCategory (category: Category): Observable<Category> {
    return this.http.put<Category>(
      Config.url + 'api/updatecategory/' + category.id,
      category, {
        ...this.httpOptions,
        responseType: 'text' as 'json'
      }
    )
  }

  public deleteCategory (id: number): Observable<void> {
    return this.http.put<void>(
      Config.url + 'api/deletecategory/' + id,
      {
        ...this.httpOptions,
        responseType: 'text' as 'json'
      }
    )
  }

}
