import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Config } from '../config/baseurl';
import { OrderDetail } from '../models/orderdetail';
import { OrderHeader } from '../models/orderheader';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    }),
  };

  public postOrderHeader(): Observable<OrderHeader> {
    return this.http.post<OrderHeader>(
      Config.url + 'api/orderheader', {
      ...this.httpOptions,
      responseType: 'text' as 'json',
    })
  }

  public doneProcessOrder(header_id: number, totalAmount: any): Observable<OrderHeader> {
    const datas = `{ "amount": ${totalAmount} }`
    return this.http.put<OrderHeader>(
      Config.url + 'api/orderheader/' + header_id, datas, {
        ...this.httpOptions,
        responseType: 'text' as 'json'
      })
  }

  public getOrderReference(): Observable<OrderHeader> {
    return this.http.get<OrderHeader>(
      Config.url + 'api/orderreference', {
      ...this.httpOptions,
    })
  }

  public addOrderDetail(orderDetail: OrderDetail): Observable<OrderDetail> {
    return this.http.post<OrderDetail>(
      Config.url + 'api/orderdetail', orderDetail,
      {
        ...this.httpOptions,
        responseType: 'text' as 'json',
      }
    )
  }

  public getOrderDetailById(header_id: number): Observable<OrderDetail[]> {
    return this.http.get<OrderDetail[]>(
      Config.url + 'api/orderdetail/' + header_id,
      this.httpOptions,
    )
  }

}
