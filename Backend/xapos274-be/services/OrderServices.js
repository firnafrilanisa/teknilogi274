const { request, response } = require("express")

module.exports = exports = (app, pool) => {
    app.post('/api/orderheader', (request, response) => {
        let dateNowMillis = Date.now();
        const query = `INSERT INTO public.order_header(
            reference, amount)
            VALUES ('${dateNowMillis}', '0');`

            pool.query(query, (error, result) => {
                if (error) {
                    response.send(400, {
                        success: false,
                        result: error,
                    });
                } else {
                    response.send(200, {
                        success: true,
                        result: `Reference created`
                    });
                }
            });
    })

    app.get('/api/orderreference', (request, response) => {
        const query = 'SELECT * FROM order_header ORDER BY reference DESC LIMIT 1'

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows[0],
                });
            }
        });
    })

    app.post('/api/orderdetail', (request, response) => {
    let { header_id, price, product_id, quantity } = request.body;
    const query = `INSERT INTO public.order_detail(
        header_id, price, product_id, quantity)
        VALUES ('${header_id}', '${price}', '${product_id}', '${quantity}');`

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: `Data Created`
                });
            }
        });
    })

    app.get('/api/orderdetail/:header_id', (request, response) => {
        let header_id = request.params.header_id;
        const query = `SELECT od.id, oh.reference, oh.id AS header_id, od.quantity, p.product_name, p.product_price AS price, p.id AS product_id
        FROM order_detail od
        JOIN order_header oh
        ON od.header_id = oh.id
        JOIN product p
        ON od.product_id = p.id
        WHERE od.header_id = ${header_id}`;

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows,
                });
            }
        });
    })

    app.put('/api/orderheader/:id', (request, response) => {
        let id = request.params.id;
        const { amount } = request.body;
        const query = `UPDATE order_header
        SET amount = ${amount}
        WHERE id = ${id}`

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: `Order Successfully`,
                });
            }
        });

    })

}