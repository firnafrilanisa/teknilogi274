const express = require('express')//untuk membuat backen
const bodyParser = require('body-parser')//adalah middleware backend dan datahost
const { request, response } = require('express')
const cors = require('cors')

// const { pool } = require('./config/dbconfig')
global.config = require('./config/dbconfig')
const app = express() //membuat var app untuk memanggil fungsi express
const port = 3000

app.use(cors())
app.use(bodyParser.json())//untuk mengubah respon jadi json
app.use(
    bodyParser.urlencoded({
        extended: true
    })
)
//baca materi node.js RESTful API & PostgreSQL
//Four of the most common HTTP methods are GET, POST, PUT, and DELETE
app.get('/', (request, response) => {//mengambil data dari '/', parameter request dan response
    //response untuk mengambil data
    response.json({ info: 'Node.js, Express, and Postgres API' })//onjek adalah keluaran data
    //objek ada di dalam kurung kurawal. key nya info dan value nya yg ada di dalam ''
})

require('./services/usersServices')(app, global.config.pool)//app dari sevice export (app,...)
require('./services/CategoryServices')(app, global.config.pool)//global.config.pool
require('./services/VariantServices')(app, global.config.pool)
require('./services/ProductServices')(app, global.config.pool)
require('./services/OrderServices')(app, global.config.pool)

app.listen(port, () => {//menjalankan server, parameter port
    console.log(`App Running on port ${port}`) //mencetak console.log
    //untuk concat ke port pakai `` yg di sebelah angka 1
})
//running setelah itu bisa di lihat ke website <http://localhost:3000/>
//server akan running terus sampai kita pencet ctrl + c untuk stop
//mendaftarkan nodemon ke index.js ke package.json
//npm i nodemon
//untuk menjalankan restart dengan nodemon maka kita menulis di package.json
//"start": "nodemon index.js" di script di bawah "test"
//kemudian terminal klik npm start

