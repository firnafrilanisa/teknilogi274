const Pool = require('pg').Pool

const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'xapos274',
    password: 'a1b2c3d4',
    port: 5432
})

module.exports = {
    pool
}